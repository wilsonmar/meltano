variables:
  PYTHONPATH: "/meltano/meltano/elt/shared_modules/:$PYTHONPATH"

stages:
  - review
  - setup
  - build
  - extract
  - model
  - update
  - review_stop

.job_template: &job_definition
  image: registry.gitlab.com/meltano/meltano-elt/extract:latest
  tags:
    - analytics

.job_housekeeping: &job_housekeeping
  image: registry.gitlab.com/meltano/meltano-elt/extract:latest
  tags:
    - housekeeping

.marketo_extract: &marketo_extract
  stage: extract
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy bash processes/mkto_processor.sh
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|marketo/

.zendesk_extract: &zendesk_extract
  stage: extract
  allow_failure: true
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/zendesk/src --schema zendesk apply_schema
    - using_proxy python3 elt/zendesk/src --schema zendesk --days=1 export
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|zendesk/

.lever_extract: &lever_extract
  stage: extract
  allow_failure: true
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/lever/src/config/required_ci_cd_vars.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/lever/src --schema lever apply_schema
    - using_proxy python3 elt/lever/src --schema lever --days=7 export
    - using_proxy python3 elt/lever/src --schema lever --days=365 --only_offers export
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|lever/

.netsuite_extract: &netsuite_extract
  stage: extract
  allow_failure: true
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/netsuite/src/config/required_ci_cd_vars.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/netsuite/src/ --schema netsuite apply_schema
    - using_proxy python3 elt/netsuite/src/ --schema netsuite export --days 3
    - using_proxy python3 elt/netsuite/src/ --schema netsuite backlog --days 15
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|netsuite/

.zuora_extract: &zuora_extract
  variables:
    FIXED_ZUORA_PASSWORD: $ZUORA_PASSWORD
  stage: extract
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - envsubst < "elt/config/environment.conf.template" > "elt/config/environment.conf"
    - using_proxy python3 elt/zuora/zuora_export.py
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|zuora/

.domains_extract: &domains_extract
  stage: extract
  retry: 1
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy bash processes/hosts_to_sfdc/python_timecheck.sh
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|domains/

.sfdc_extract: &sfdc_extract
  variables:
    FIXED_SFDC_PASSWORD: $SFDC_PASSWORD
  stage: extract
  retry: 1
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - envsubst < "elt/config/kettle/kettle.properties" > "$KETTLE_HOME/.kettle/kettle.properties"
    - cp "elt/config/kettle/repositories.xml" "$KETTLE_HOME/.kettle/repositories.xml"
    - using_proxy kitchen.sh -file=elt/sfdc/sfdc_extract.kjb -level=Minimal
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|sfdc/

.stripe_extract_githost: &stripe_extract_githost
  stage: extract
  allow_failure: true
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/stripe/src --schema stripe_githost apply_schema
    - using_proxy python3 elt/stripe/src --schema stripe_githost --days=7 export
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|stripe_githost/

.stripe_extract_about_gitlab: &stripe_extract_about_gitlab
  stage: extract
  allow_failure: true
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/stripe/src --schema stripe_about_gitlab apply_schema
    - using_proxy python3 elt/stripe/src --schema stripe_about_gitlab --days=7 export
    - stop_cloudsqlproxy
  except:
    variables:
      - $EXTRACT_SKIP =~ /all|stripe_gitlab/

# Stage: extract

## Production Jobs

sfdc:
  <<: *sfdc_extract
  only:
    - master

zuora:
  <<: *zuora_extract
  only:
    - master

pings:
  stage: extract
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/pings/src/config/required_vars_pings.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/pings/src apply_schema --db_manifest version
    - using_proxy python3 elt/pings/src export --db_manifest version --days 1
    - stop_cloudsqlproxy
  only:
    - master

ci_stats:
  stage: extract
  allow_failure: true
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/pings/src/config/required_vars_ci_stats.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/pings/src apply_schema --db_manifest ci_stats
    - using_proxy python3 elt/pings/src export --db_manifest ci_stats --hours 8
    - stop_cloudsqlproxy
  only:
    - master

spreadsheet_loader_manual:
  stage: extract
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - sleep 20
    - pip3 install -U pyasn1-modules
    - python3 elt/util/spreadsheet_loader.py sheet historical.sales_quota
    - stop_cloudsqlproxy
  when: manual

spreadsheet_loader:
  stage: extract
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - sleep 20
    - pip3 install -U pyasn1-modules
    - python3 elt/util/spreadsheet_loader.py sheet historical.sales_quota
    - stop_cloudsqlproxy
  only:
    - master

domains:
  <<: *domains_extract
  only:
    - master

marketo:
  <<: *marketo_extract
  only:
    - master

zendesk:
  <<: *zendesk_extract
  retry: 1
  only:
    - master

lever:
  <<: *lever_extract
  retry: 1
  only:
    - master

netsuite:
  <<: *netsuite_extract
  retry: 1
  only:
    - master

netsuite_backlog_manual:
  stage: extract
  allow_failure: true
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/netsuite/src/config/required_ci_cd_vars.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/netsuite/src/ --schema netsuite backlog --days 60
    - stop_cloudsqlproxy
  retry: 1
  when: manual

stripe_extract_about_gitlab_manual:
  variables:
    ABOUT_GITLAB_STRIPE_API_KEY_SK: $ABOUT_GITLAB_STRIPE_API_KEY_SK
  <<: *job_definition
  retry: 1
  only:
    - master

stripe_extract_githost_manual:
  variables:
    GITHOST_STRIPE_API_KEY_SK: $GITHOST_STRIPE_API_KEY_SK
  <<: *job_definition
  retry: 1
  only:
    - master

## Review Jobs

sfdc_manual:
  <<: *sfdc_extract
  only:
    - branches
  except:
    - master
  when: manual

zuora_manual:
  <<: *zuora_extract
  only:
    - branches
  except:
    - master
  when: manual

pings_manual:
  stage: extract
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/pings/src/config/required_vars_pings.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/pings/src apply_schema --db_manifest version
    - using_proxy python3 elt/pings/src export --db_manifest version --days 10
    - stop_cloudsqlproxy
  only:
    - branches
  except:
    - master
  when: manual

pings_backfill:
  stage: extract
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/pings/src/config/required_vars_pings.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/pings/src apply_schema --db_manifest version
    - using_proxy python3 elt/pings/src export --db_manifest version
    - stop_cloudsqlproxy
  only:
    - master
  when: manual

ci_stats_manual:
  stage: extract
  <<: *job_definition
  script:
    - python3 ci_scripts/check_ci_cd_vars.py --file elt/pings/src/config/required_vars_ci_stats.yaml
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/pings/src apply_schema --db_manifest ci_stats
    - using_proxy python3 elt/pings/src export --db_manifest ci_stats --hours 8
    - stop_cloudsqlproxy
  only:
    - branches
  except:
    - master
  when: manual

domains_manual:
  <<: *domains_extract
  only:
    - branches
  except:
    - master
  when: manual

marketo_manual:
  <<: *marketo_extract
  only:
    - branches
  except:
    - master
  when: manual

zendesk_manual:
  <<: *zendesk_extract
  retry: 1
  only:
    - branches
  except:
    - master
  when: manual

lever_manual:
  <<: *lever_extract
  retry: 1
  only:
    - branches
  except:
    - master
  when: manual

netsuite_manual:
  <<: *netsuite_extract
  retry: 1
  only:
    - branches
  except:
    - master
  when: manual

stripe_extract_githost_manual:
  <<: *stripe_extract_githost
  retry: 1
  only:
    - branches
  except:
    - master
  when: manual

stripe_extract_about_gitlab_manual:
  <<: *stripe_extract_about_gitlab
  retry: 1
  only:
    - branches
  except:
    - master
  when: manual
# Stage: model

dbt:
  stage: model
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - cd elt/dbt
    - dbt -d run --profiles-dir profile --target prod 
    - dbt -d test --profiles-dir profile --target prod 
    - stop_cloudsqlproxy

dbt_refresh:
  stage: model
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - cd elt/dbt
    - dbt -d run --profiles-dir profile --target prod --full-refresh
    - dbt -d test --profiles-dir profile --target prod 
    - stop_cloudsqlproxy
  when: manual

# Stage: update

sfdc_update:
  stage: update
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 processes/sfdc_processor.py
    - stop_cloudsqlproxy
  except:
    variables:
      - $UPDATE_SKIP =~ /all|sfdc_update/

sfdc_snapshot:
  stage: update
  <<: *job_definition
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 elt/util/snapshot.py
    - stop_cloudsqlproxy
  except:
    variables:
      - $UPDATE_SKIP =~ /all|sfdc_snapshot/

# Stage: review

review: &review
  stage: review
  image: google/cloud-sdk:latest
  variables:
    GIT_STRATEGY: none
  tags:
    - analytics
  script:
    - set_sql_instance_name
    - manage_review_cloudsql
  environment:
    name: review/$CI_COMMIT_REF_NAME
    on_stop: review_stop
  only:
    - branches
  except:
    - master

review_refresh:
  <<: *review
  variables:
    GIT_STRATEGY: none
    FORCE: "true"
  when: manual

# Stage: review_stop

review_stop:
  stage: review_stop
  image: google/cloud-sdk:latest
  variables:
    GIT_STRATEGY: none
  script:
    - set_sql_instance_name
    - delete_review_cloudsql
  when: manual
  allow_failure: true
  only:
    - branches
  except:
    - master
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop

# Stage: setup

refresh_dev: &refresh_dev
  <<: *job_housekeeping
  stage: setup
  variables:
    GCP_INSTANCE_NAME: $GCP_PRODUCTION_INSTANCE_NAME
    GIT_STRATEGY: clone # TODO make the script part of the container
  script:
    - refresh_dev_cloudsql
    - setup_cloudsqlproxy $GCP_DEV_INSTANCE_NAME
    - using_proxy python3 ci_scripts/grant_roles.py
    - stop_cloudsqlproxy
  only:
    - master

force_refresh_dev:
  <<: *refresh_dev
  variables:
    GCP_INSTANCE_NAME: $GCP_PRODUCTION_INSTANCE_NAME
    GIT_STRATEGY: clone # TODO make the script part of the container
    GIT_DEPTH: 1
    FORCE: "true"
  when: manual

update_db: &update_db
  <<: *job_housekeeping
  stage: setup
  script:
    - set_sql_instance_name
    - setup_cloudsqlproxy
    - using_proxy python3 ci_scripts/grant_roles.py
    - stop_cloudsqlproxy
  only:
    - branches
  except:
    - master

force_update_db:
  <<: *update_db
  when: manual

# ---------------------------------------------------------------------------

.meltano: &meltano |

  function auth_gcloud() {
    if [ ${GCP_AUTH:=1} = 0 ]; then
      # already authenticated
      return $GCP_AUTH
    fi

    if [ -n "$GCP_SERVICE_CREDS" ]; then
      echo $GCP_SERVICE_CREDS > gcp_credentials.json
      gcloud auth activate-service-account --key-file=gcp_credentials.json
      export GCP_AUTH=0
    else
      echo "No credentials provided."
      export GCP_AUTH=1
    fi

    return $GCP_AUTH
  }

  function find_sql_instance() {
    if [ "$CI_COMMIT_REF_NAME" = "master" ]; then
      export GCP_INSTANCE_NAME=$GCP_PRODUCTION_INSTANCE_NAME
      return 0
    fi

    if auth_gcloud; then
      INSTANCE=$(gcloud sql instances list --project "$GCP_PROJECT" | \
                 grep "$GCP_INSTANCE_REF_SLUG" | \
                 tail -n1 | \
                 cut -d' ' -f1)
      INSTANCE_EXISTS=${PIPESTATUS[1]}

      if $INSTANCE_EXISTS && [ -n "$INSTANCE" ]; then
        export GCP_INSTANCE_NAME="$INSTANCE"
        return 0
      fi

      return 1
    fi
  }

  function set_sql_instance_name() {
    if auth_gcloud; then
      # 95 (max) - 10 (job_id) - 2 (`-`) = 83
      SLUG_LENGTH=$(( 83 - ${#CI_PROJECT_NAME} ))
      export GCP_INSTANCE_REF_SLUG=$CI_PROJECT_NAME-${CI_COMMIT_REF_SLUG:0:$SLUG_LENGTH}
      export JOB_GCP_INSTANCE_NAME=$GCP_INSTANCE_REF_SLUG-${CI_JOB_ID:0:10}

      if find_sql_instance; then
        echo "Found instance with name: $GCP_INSTANCE_NAME"
      fi
    fi
  }

  function gcloud_wait() {
    OPERATION_ID=$(gcloud $* | \
                  grep "^name: " | \
                  cut -d' ' -f2) # name: <operation_id>

    if [ -n "$OPERATION_ID" ]; then
        echo "Waiting for operation $OPERATION_ID."
        gcloud beta $1 operations wait "$OPERATION_ID"
    else
      echo "Cannot fetch OPERATION_ID, make sure to pass the `--async` option to gcloud."
      return 1
    fi

    return 0
  }

  function manage_review_cloudsql() {
    # Check to see if branch name equals the production EDW instance name
    if [ "$GCP_INSTANCE_NAME" = "$GCP_PRODUCTION_INSTANCE_NAME" ]; then
      echo "The branch name cannot match the production EDW instance name."
      return 1
    fi

    if auth_gcloud; then
      if [ -n "$GCP_INSTANCE_NAME" ]; then # instance exists
        if [ "$FORCE" = "true" ]; then
          # delete the old instances
          echo "Cleaning up old Cloud SQL instances for this branch."
          IFS=$'\n'
          for instance in $(gcloud sql instances list --project "$GCP_PROJECT" --filter "$GCP_INSTANCE_REF_SLUG" | tail -n +2 | cut -d' ' -f1); do
            echo "Deleting instance $instance."
            gcloud sql instances delete --async --quiet --project "$GCP_PROJECT" "$instance"
          done
        else
          echo "Instance is available at $GCP_INSTANCE_NAME"
          return 0
        fi
      fi

      export GCP_INSTANCE_NAME=$JOB_GCP_INSTANCE_NAME

      # Clone new instance and wait for it
      echo "Cloning new instance $GCP_INSTANCE_NAME."
      gcloud_wait sql instances clone --async --project "$GCP_PROJECT" "$GCP_PRODUCTION_INSTANCE_NAME" "$JOB_GCP_INSTANCE_NAME"
    fi

    return 0
  }

  function delete_review_cloudsql() {
    # Check to see if branch name equals the production EDW instance name
    if [ "$GCP_INSTANCE_NAME" = "$GCP_PRODUCTION_INSTANCE_NAME" ]; then
      echo "The branch name cannot match the production EDW instance name."
      return 1
    fi

    if auth_gcloud; then
      gcloud sql instances delete --async --project "$GCP_PROJECT" "$GCP_INSTANCE_NAME"
    fi
  }

  function setup_cloudsqlproxy() {
    INSTANCE_NAME=${1:-$GCP_INSTANCE_NAME}
    if auth_gcloud; then
      cloud_sql_proxy -instances="$GCP_PROJECT:$GCP_REGION:$INSTANCE_NAME"=tcp:5432 -credential_file=gcp_credentials.json -verbose=False &
    fi
  }

  function using_proxy() {
    ci_scripts/wait-for-it.sh localhost:5432 -q -s --timeout=60 -- $*
  }

  function stop_cloudsqlproxy() {
    kill -9 $(pgrep cloud_sql_proxy)
  }

  function refresh_dev_cloudsql() {
    # Get current UTC time
    H=$(date -u +%H)

    # Checks if between 4 and 7 AM UTC (11PM-2AM CST)
    # Prod backups happen between 8-12 AM UTC (3-7 AM CST)
    # 10#$H returns the hour in base 10
    if [ "$FORCE" = "true" ] || (( 4 <= 10#$H && 10#$H < 7 )); then
      if auth_gcloud; then
        echo "Restoring dev instance from latest successful prod backup."
        gcloud config set project "$GCP_PROJECT"
        gcloud sql backups list --instance "$GCP_INSTANCE_NAME" | grep SUCCESSFUL | head -n 1 | cut -d' ' -f1 | xargs -I % gcloud sql backups restore % --restore-instance="$GCP_DEV_INSTANCE_NAME" --backup-instance="$GCP_INSTANCE_NAME"
      fi
    else
        echo "Dev instance refresh not run: Only run between the hours of 4 AM UTC and 7 AM UTC"
    fi
  }


before_script:
  - *meltano
